# FreedomBox Cloud Image Builder

Build images of FreedomBox for various cloud providers.

## Supported cloud service providers
- AWS EC2

## Tools used

- Hashicorp Packer + Ansible
